package com.hcl.mortgage.dao;

import java.util.List;

import com.hcl.mortgage.model.Mortgage;

public interface ExistingLoanServiceDao {

	public List<Mortgage> getMortgageDetails(String userId);
}
