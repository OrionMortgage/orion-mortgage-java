package com.hcl.mortgage.model;

public class Mortgage {

	private int mid;
	private int uid;
	private String propertyType;
	private double marketValue;
	private double loanAmount;
	private int tenure;//
	
	private double rateOfInterest;
	private int emi;
	private double creditScore;
	
	private boolean mortgageFlag;
	private boolean securityFlag;
	private boolean legalResFlag;
	
	private String status;
	private String modifyBy;
	private String modifyDT;
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getPropertyType() {
		return propertyType;
	}
	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}
	public double getMarketValue() {
		return marketValue;
	}
	public void setMarketValue(double marketValue) {
		this.marketValue = marketValue;
	}
	public double getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}
	public int getTenure() {
		return tenure;
	}
	public void setTenure(int tenure) {
		this.tenure = tenure;
	}
	public double getRateOfInterest() {
		return rateOfInterest;
	}
	public void setRateOfInterest(double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}
	public int getEmi() {
		return emi;
	}
	public void setEmi(int emi) {
		this.emi = emi;
	}
	public double getCreditScore() {
		return creditScore;
	}
	public void setCreditScore(double creditScore) {
		this.creditScore = creditScore;
	}
	public boolean isMortgageFlag() {
		return mortgageFlag;
	}
	public void setMortgageFlag(boolean mortgageFlag) {
		this.mortgageFlag = mortgageFlag;
	}
	public boolean isSecurityFlag() {
		return securityFlag;
	}
	public void setSecurityFlag(boolean securityFlag) {
		this.securityFlag = securityFlag;
	}
	public boolean isLegalResFlag() {
		return legalResFlag;
	}
	public void setLegalResFlag(boolean legalResFlag) {
		this.legalResFlag = legalResFlag;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getModifyBy() {
		return modifyBy;
	}
	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}
	public String getModifyDT() {
		return modifyDT;
	}
	public void setModifyDT(String modifyDT) {
		this.modifyDT = modifyDT;
	}
	@Override
	public String toString() {
		return "Mortgage [mid=" + mid + ", uid=" + uid + ", propertyType=" + propertyType + ", marketValue="
				+ marketValue + ", loanAmount=" + loanAmount + ", tenure=" + tenure + ", rateOfInterest="
				+ rateOfInterest + ", emi=" + emi + ", creditScore=" + creditScore + ", mortgageFlag=" + mortgageFlag
				+ ", securityFlag=" + securityFlag + ", legalResFlag=" + legalResFlag + ", status=" + status
				+ ", modifyBy=" + modifyBy + ", modifyDT=" + modifyDT + "]";
	}
	
	
} 
