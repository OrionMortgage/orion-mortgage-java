package com.hcl.mortgage.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.hcl.mortgage.common.QueriesUtil;
import com.hcl.mortgage.model.Customer;
import com.hcl.mortgage.model.Mortgage;

public class ExistingLoanServiceDaoImpl implements ExistingLoanServiceDao {

	private static final Logger logger = Logger.getLogger(ExistingLoanServiceDaoImpl.class);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private NamedParameterJdbcTemplate namedTemplate;

	@Override
	public List<Mortgage> getMortgageDetails(String userId) {

		logger.info("---ExistingLoanServiceDaoImpl - getMortgageDetails()");
		logger.info("Executing query- " + QueriesUtil.SELECT_ALL_MORTGAGE_USERID);

		List<Mortgage> existingMortgages = new ArrayList<>();
		
		List<Map<String,Object>> existingMortgageList = jdbcTemplate.queryForList(QueriesUtil.SELECT_ALL_MORTGAGE_USERID, new Object[] { userId});
		for(Map existingMortgageEntry : existingMortgageList) {
			Mortgage singleMortgage = new Mortgage();
			singleMortgage.setUid((int) existingMortgageEntry.get("USER_ID"));
			singleMortgage.setMid((int) existingMortgageEntry.get("ID"));
			
			existingMortgages.add(singleMortgage);
		}
//		List<Mortgage> existingMortgageList1 = jdbcTemplate.query(QueriesUtil.SELECT_ALL_MORTGAGE_USERID, new BeanPropertyRowMapper(Customer.class));
//		jdbcTemplate.query(QueriesUtil.SELECT_ALL_MORTGAGE_USERID, new BeanPropertyRowMapper(Mortgage.class));
		

		return existingMortgages;
	}

}
