package com.hcl.mortgage.controller;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.mortgage.model.Customer;
import com.hcl.mortgage.services.CustomerService;

//@CrossOrigin
@RestController

public class CustomerController {

	protected Logger logger;
	
	public CustomerController() {
	  logger=Logger.getLogger(CustomerController.class);
	}
    
  @Autowired
  CustomerService customerService;
	
//consumes = {"application/json"}
  
@RequestMapping(value="custDetails/create",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)//==> URI template variables are "empName" and "empId" 
public String saveCustDetails(@RequestBody Customer customer){
	 logger.info("saveCustDetails()....!");
	
    return customerService.saveCustDetails(customer); // this is page name
}


@RequestMapping(value="custDetails/create/demo",method=RequestMethod.POST,params={"name","phone"})//==> URI template variables are "empName" and "empId" 
public String saveCustDetails(@RequestParam String name,@RequestParam long phone){
	 logger.info("saveCustDetails()....! : name ="+name+" phone :="+phone);
	 Customer c =new Customer();
	 c.setName(name);
	 c.setPhone(phone);
    return customerService.saveCustDetails(c); // this is page name
}

@RequestMapping(value ="getCustomersList", method=RequestMethod.GET,produces="application/json")

public List<Customer> getAllCustomers(){
	 logger.info("getAllCustomers()....CustomerController");
	 
   return customerService.getAllCustomers(); 
}

@CrossOrigin(value = "*")
@RequestMapping(value = "/test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public Test test(){
	Test t = new Test();
	t.setEmail("lingamurthy");
	t.setName("xyz");
	return t;
}




















/*A) here URI template variables {empName} and {empId} name not matched with  method parameter "String name" & "String id" so in
 *   @PathVariable("var_name"), 'value' element is required to bound URI template variable 
 */
@RequestMapping(value="/emp_Details/{empName}/{empId}")//==> URI template variables are "empName" and "empId" 
public String getDetails(@PathVariable("empName") String name,@PathVariable("empId") String id,Model model){
	
	 logger.info("getDetails()....!");
	 
	 logger.info("employee Name :"+name);
	 logger.info("Employee ID:"+id);
	 
	 model.addAttribute("empName", name);
	 model.addAttribute("empId", id);
	 
   return "success"; // this is page name
}
/*
@RequestMapping("/view_Details/{empName}/{empId}")
public String viewDetails(@PathVariable Map<String, String> map,Model model) {
	    logger.info("viewDetails()......!!!");
	    
	    String empName = map.get("empName");
	    String empId = map.get("empId");
	    
	    logger.info("employee Name :"+empName +" employee ID:"+empId);
	    Map<String, String> m=new HashMap<>();
	    m.put("empName", model.get("empName"));
	    
	    m.put("empId",empName);;
	    
	    model.addAttribute("empName",empName);
	    model.addAttribute("empId", empId);
	  
   return "success";
}

@RequestParam : Used bind request parameter to method parameter

A) If request parameter name matched with method parameter name then no need to provide value in @RequestParam annotation 

@RequestMapping("/request_param_details")
public String getRequestParamDetail(@RequestParam String name, @RequestParam String id, @RequestParam String state, Model model){
	  logger.info("getRequestParamDetail().......!");
	 
   //get the value from request parameter & save into model 
	model.addAttribute("name", name);
	model.addAttribute("id", id);
	model.addAttribute("state", state);
	
	return "success";
}

B) If request parameter name not matched with method parameter name then you must provide value{which specify name of request parameter} 
 * element in @RequestParam annotation 

@Autowired
  HashMap<String, String> map;

@RequestMapping("/get_req_param_details")
public ModelAndView getRequestParamDetail(@RequestParam("name") String userName, @RequestParam("id") String userId, @RequestParam("state") String stateName){
	  logger.info("getRequestParamDetail().......!");
	 
	map.put("name", userName);
	map.put("id", userId);
	map.put("state", stateName);
	
	return new ModelAndView("success", map);
}


@RequestMapping("/req_param_details")
public String getRequestParamDetail(@RequestParam Map<String, String> queryMap,Model model){
	  logger.info("getRequestParamDetail().......Using Map");
	
	 String name=queryMap.get("name"); 
	 String id=queryMap.get("id");
	 String state=queryMap.get("state");
      
	 logger.info("UserName :"+name+" ID:"+id+" State :"+state);
	 
	 model.addAttribute("name", name);
	 model.addAttribute("id", id);
	 model.addAttribute("state", state);
	 
	return "success";
}

@RequestMapping(value="/req_param_details", params={"name","dept","state"})
public String getRequestParamDetails(@RequestParam String name,@RequestParam("dept")String deptName,@RequestParam("state")String stateName,Model model){
	  logger.info("getRequestParamDetail().......Using @RequestMapping with params attribute");
	 
	 logger.info("Name :"+name +" DeptName :"+deptName+" State:"+stateName);
	  
  	 model.addAttribute("name", name);
	 model.addAttribute("id", deptName);
	 model.addAttribute("state", stateName);
		 
	return "success";
}

@RequestMapping("/other_handler_method.do")
public String getRedirect_to_handlerMehod(Model model){
	
	logger.info("getRedirect_to_handlerMehod():HelloWorldController");
	
	model.addAttribute("message", "using redirect to other controller's handler merhod");
	
  return "operations";	
}
*/

}

class Test implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		return "Test [name=" + name + ", email=" + email + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Test other = (Test) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	private String name;
	private String email;
}