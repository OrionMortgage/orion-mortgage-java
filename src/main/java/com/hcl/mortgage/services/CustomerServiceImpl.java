package com.hcl.mortgage.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.mortgage.dao.CustomerDao;
import com.hcl.mortgage.model.Customer;

@Service("customerService")
public class CustomerServiceImpl implements CustomerService {
    
	private static final Logger logger =Logger.getLogger(CustomerServiceImpl.class);
	
	 @Autowired
	 private CustomerDao customerDao;

	@Override
	public String saveCustDetails(Customer customer) {
		
		logger.debug("saveCustDetails() : CustomerServiceImpl");
		
		return customerDao.saveCustDetails(customer);
	}

	@Override
	public List<Customer> getAllCustomers() {
		// TODO Auto-generated method stub
		return customerDao.getAllCustomers();
	}
	
	

}
