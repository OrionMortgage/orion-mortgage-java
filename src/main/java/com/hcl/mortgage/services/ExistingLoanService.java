package com.hcl.mortgage.services;

import java.util.List;

import com.hcl.mortgage.model.Mortgage;

public interface ExistingLoanService {

	public List<Mortgage> getExistingLoanDetails(String userId);
}
