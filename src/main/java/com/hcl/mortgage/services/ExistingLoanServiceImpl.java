package com.hcl.mortgage.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.mortgage.dao.ExistingLoanServiceDao;
import com.hcl.mortgage.model.Mortgage;

@Service("existingLoanService")
public class ExistingLoanServiceImpl implements ExistingLoanService {

	private static final Logger logger = Logger.getLogger(ExistingLoanServiceImpl.class);
	
	@Autowired
	ExistingLoanServiceDao existingLoanServiceDao;
	
	@Override
	public List<Mortgage> getExistingLoanDetails(String userId) {
		
		logger.debug("Inside ExistingLoanServiceImpl - getExistingLoanDetails");
		
		existingLoanServiceDao.getMortgageDetails(userId);
		
		return null;
	}

}
