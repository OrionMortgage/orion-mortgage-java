package com.hcl.mortgage.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.mortgage.model.ExistingLoanRequestModel;
import com.hcl.mortgage.model.Mortgage;
import com.hcl.mortgage.services.ExistingLoanService;

@RestController
public class ExistingLoanController {

	protected Logger logger;

	public ExistingLoanController() {
		logger = Logger.getLogger(ExistingLoanController.class);
	}

	@Autowired
	ExistingLoanService existingLoanService;

	@RequestMapping(value = "getExistingLoanDetails", method = RequestMethod.POST, produces = "application/json")
	public List<Mortgage> getExistingLoanDetails(@RequestBody ExistingLoanRequestModel existingLoanRequestModel) {

		logger.info("---Inside ExistingLoanController - getExistingLoanDetails()---");
		return existingLoanService.getExistingLoanDetails(existingLoanRequestModel.getUserId());
	}

}
