package com.hcl.mortgage.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.hcl.mortgage.common.QueriesUtil;
import com.hcl.mortgage.model.Customer;

@Repository("customerDao")
public class CustomerDaoImpl implements CustomerDao {
	
	private static final Logger logger=Logger.getLogger(CustomerDaoImpl.class);
    
   @Autowired
	private JdbcTemplate jdbcTemplate;
    
	@Autowired
	private NamedParameterJdbcTemplate namedTemplate;
		
	@Override
	public String saveCustDetails(Customer customer) {
		
		customer.setEmail("abc@gmail.com");
		customer.setLoanRequestedAmount(2000.0);
		logger.debug("saveCustDetails(Insert_Q) :"+QueriesUtil.INSERT_Q);
		
		logger.debug("saveCustDetails() :CustomerDaoImpl :"+customer);
		
		KeyHolder keyHolder =new GeneratedKeyHolder();
		String inputParamNames[]={"CID"};
		
		int retVal=namedTemplate.update(QueriesUtil.INSERT_Q, new BeanPropertySqlParameterSource(customer),keyHolder,inputParamNames);  
		
		int getKey=keyHolder.getKey().intValue();
		
		logger.debug("Return Value :"+retVal +" Key :"+getKey);
		logger.debug("retVal ::"+retVal);
		
	    long genrtdCID =keyHolder.getKey().longValue();
		
		if(retVal >0){
			logger.debug("Record Inserted successfully into Customer ...: retVal :="+retVal +" generatedKey :="+genrtdCID);
		}else {
			logger.debug("Insertion  Unsuccessful (CPMTRAN)  : retVal #"+retVal);
		}
		logger.debug("End:saveOrUpdateCpmTransport() : retVal :="+retVal);
	  
		return Long.toString(genrtdCID);
	}

	@Override
	public List<Customer> getAllCustomers() {
		
	    logger.debug("getAllCustomers(Select_Q) :"+QueriesUtil.SELECT_Q);
	    
	    List<Customer> list=new ArrayList<>();
	    int rowCount=0;
	    
   SqlRowSet rowSet = namedTemplate.queryForRowSet(QueriesUtil.SELECT_Q,new MapSqlParameterSource());
		
		//Iterate rowSet
		while (rowSet.next()) {
			Customer c=new Customer();
		    c.setCid(rowSet.getInt("CID"));
		    c.setName(rowSet.getString("NAME"));
		    c.setPhone(rowSet.getLong("PHONE"));
		    c.setEmail(rowSet.getString("EMAIL"));
		    c.setLoanRequestedAmount(rowSet.getLong("LOAN_REQTED_AMOUNT"));
		    c.setSequrityAmount(rowSet.getLong("SEQURITY_AMT"));
		    
		  /*  e.setName(rowSet.getString("NAME"));
		    e.setPhone(rowSet.getLong("PHONE"));
		    e.setEmail(rowSet.getString("EMAIL"));
		    
		   list.add(e); */
		   rowCount++;
		}
		
		logger.debug("Employee List :"+list);
		logger.debug("Total Row Count :"+rowCount);
		
		return list;
		
	}

}

