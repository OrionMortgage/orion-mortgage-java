package com.hcl.mortgage.utility;

public class MortgageUtility {
  
  //UserType	
	public static final String  NEW ="NEW";
	public static final String NORMAL ="NORMAL";
	public static final String PREMIUM ="PREMIUM";
	
   //PropertyType	
	public static final String  EXISTING ="EXISTING";
	public static final String  RESIDENCE ="RESIDENCE";
	
	//UserRole
	public static final String  CUSTOMER ="CUSTOMER";
	public static final String  EMPLOYEE ="EMPLOYEE";
	
	//Flag
	public static final boolean  TRUE =true;
	public static final boolean  FALSE =false;
	
	private MortgageUtility(){
		
	}
	 
}
