package com.hcl.mortgage.common;

public class QueriesUtil{

	private QueriesUtil(){
		
	}
	public static final String INSERT_Q="INSERT INTO CUSTOMER_DETAILS (CID,NAME,PHONE,EMAIL,LOAN_REQTED_AMOUNT,SEQURITY_AMT,LOAN_TYPE,STATUS) VALUES"
			+ "(:cid,:name,:phone,:email,:loanRequestedAmount,:sequrityAmount,:loanType,:status)";
	
	public static final String SELECT_Q="SELECT * CUSTOMER_DETAILS";
	
	public static final String SELECT_ALL_MORTGAGE_USERID = "SELECT * MORTGAGE WHERE USER_ID = ?";
	
}
