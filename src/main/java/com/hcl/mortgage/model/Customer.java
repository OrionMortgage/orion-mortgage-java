package com.hcl.mortgage.model;

public class Customer {

	private int cid;
	private String name;
	private long phone;
	private String email;
	private double loanRequestedAmount; //LOAN_REQTED_AMOUNT
	private double sequrityAmount; //SEQURITY_AMT
	private String loanType;
	private String status;
	//dummy check
	
	
	
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getPhone() {
		return phone;
	}
	public void setPhone(long phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public double getLoanRequestedAmount() {
		return loanRequestedAmount;
	}
	public void setLoanRequestedAmount(double loanRequestedAmount) {
		this.loanRequestedAmount = loanRequestedAmount;
	}
	public double getSequrityAmount() {
		return sequrityAmount;
	}
	public void setSequrityAmount(double sequrityAmount) {
		this.sequrityAmount = sequrityAmount;
	}
	public String getLoanType() {
		return loanType;
	}
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Customer [name=" + name + ", phone=" + phone + ", email=" + email + ", loanRequestedAmount="
				+ loanRequestedAmount + ", sequrityAmount=" + sequrityAmount + ", loanType=" + loanType + "]";
	}
	
}
