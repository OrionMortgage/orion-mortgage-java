package com.hcl.mortgage.dao;

import java.util.List;

import com.hcl.mortgage.model.Customer;

public interface CustomerDao {

	public String saveCustDetails(Customer customer);

	public List<Customer> getAllCustomers();
	
}
