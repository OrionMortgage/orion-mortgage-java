package com.hcl.mortgage.model;

public class ExistingLoanRequestModel {

	private String userId;
	private String roleId;
	
	public String getRoleId() {
		return roleId;
	}
	public String getUserId() {
		return userId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
}
