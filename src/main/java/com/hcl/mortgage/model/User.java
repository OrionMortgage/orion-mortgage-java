package com.hcl.mortgage.model;

public class User {

	private int uid;
	private String userName;
	private String userType;
	private String userRole;
	private int age; 
	private long mobile;
	private String email;
	private String modifyDT;
	private String modifyBy;
	private int addId;//address
	
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public long getMobile() {
		return mobile;
	}
	public void setMobile(long mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getModifyDT() {
		return modifyDT;
	}
	public void setModifyDT(String modifyDT) {
		this.modifyDT = modifyDT;
	}
	public String getModifyBy() {
		return modifyBy;
	}
	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}
	public int getAddId() {
		return addId;
	}
	public void setAddId(int addId) {
		this.addId = addId;
	}
	@Override
	public String toString() {
		return "User [userName=" + userName + ", userType=" + userType + ", userRole=" + userRole + ", age=" + age
				+ ", mobile=" + mobile + ", email=" + email + ", modifyDT=" + modifyDT + ", modifyBy=" + modifyBy + "]";
	}
	
   		
}
