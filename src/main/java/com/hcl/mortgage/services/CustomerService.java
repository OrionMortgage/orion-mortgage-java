package com.hcl.mortgage.services;

import java.util.List;

import com.hcl.mortgage.model.Customer;

public interface CustomerService {

	 public String saveCustDetails(Customer customer);

	public List<Customer> getAllCustomers();

}
	
